﻿#pragma once


template<size_t Rows, size_t Columns, size_t Area = Rows * Columns>
class ILife
{
public:
	virtual ~ILife() = default;

	virtual void Step() = 0;

	virtual void Clear() = 0;
};
