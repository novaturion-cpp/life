﻿#pragma once
#include "ilife.hpp"


template<size_t Rows, size_t Columns, size_t Area = Rows * Columns>
class LifeArray : public ILife<Rows, Columns>
{
public:
	LifeArray() = default;

	virtual ~LifeArray() = default;

public:
	virtual void Step()
	{
		auto nextPointer = std::make_unique<std::array<uint8_t, Area>>();
		auto& next = *nextPointer.get();
		auto& grid = *Grid.get();

		for (auto i = 1; i < Rows - 1; ++i)
		{
			for (auto j = 1; j < Columns - 1; ++j)
			{
				const auto aliveNeighbours =
					grid[(i - 1) * Columns + j - 1] +
					grid[(i - 1) * Columns + j] +
					grid[(i - 1) * Columns + j + 1] +
					grid[i * Columns + j - 1] +
					grid[i * Columns + j + 1] +
					grid[(i + 1) * Columns + j - 1] +
					grid[(i + 1) * Columns + j] +
					grid[(i + 1) * Columns + j + 1];

				const auto cell = grid[i * Columns + j];
				const auto keep = cell && (aliveNeighbours == 2 || aliveNeighbours == 3);
				const auto makeNew = !cell && aliveNeighbours == 3;

				next[i * Columns + j] = keep || makeNew;
			}
		}

		grid = next;
	}

	virtual void Clear()
	{
		Grid.release();
		Grid = std::make_unique<std::array<uint8_t, Area>>();
	}

protected:
	std::unique_ptr<std::array<uint8_t, Area>> Grid = std::make_unique<std::array<uint8_t, Area>>();
};
