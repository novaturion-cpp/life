﻿#pragma once
#include "ilife.hpp"


template<size_t Rows, size_t Columns, size_t Area = Rows * Columns>
class LifeArray2D : public ILife<Rows, Columns>
{
public:
	LifeArray2D() = default;

	virtual ~LifeArray2D() = default;

public:
	virtual void Step()
	{
		auto nextPointer = std::make_unique<std::array<std::array<uint8_t, Columns>, Rows>>();
		auto& next = *nextPointer.get();
		auto& grid = *Grid.get();

		for (auto i = 1; i < Rows - 1; ++i)
		{
			for (auto j = 1; j < Columns - 1; ++j)
			{
				const auto aliveNeighbours =
					grid[(i - 1)][j - 1] +
					grid[(i - 1)][j] +
					grid[(i - 1)][j + 1] +
					grid[i][j - 1] +
					grid[i][j + 1] +
					grid[(i + 1)][j - 1] +
					grid[(i + 1)][j] +
					grid[(i + 1)][j + 1];

				const auto cell = grid[i][j];
				const auto keep = cell && (aliveNeighbours == 2 || aliveNeighbours == 3);
				const auto makeNew = !cell && aliveNeighbours == 3;

				next[i][j] = keep || makeNew;
			}
		}

		grid = next;
	}

	virtual void Clear()
	{
		Grid.release();
		Grid = std::make_unique<std::array<std::array<uint8_t, Columns>, Rows>>();
	}

protected:
	std::unique_ptr<std::array<std::array<uint8_t, Columns>, Rows>> Grid =
		std::make_unique<std::array<std::array<uint8_t, Columns>, Rows>>();
};
