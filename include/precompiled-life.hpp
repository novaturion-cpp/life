#pragma once

// Exclude rarely-used stuff from Windows headers
#define WIN32_LEAN_AND_MEAN

#include <array>
#include <bitset>
#include <chrono>
#include <iostream>
