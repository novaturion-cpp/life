#include "precompiled-life.hpp"

#include "life/life-array-2d.hpp"
#include "life/life-array.hpp"
#include "life/life-bitset.hpp"


template<size_t Rows, size_t Columns, size_t Area = Rows * Columns>
void RUN_LIFE(
	std::unique_ptr<ILife<Rows, Columns>>& life,
	const size_t steps = 1000
)
{
	const auto start = std::chrono::high_resolution_clock::now();
	for (auto i = 0; i < steps; ++i)
	{
		life->Step();
	}
	const auto end = std::chrono::high_resolution_clock::now();

	const auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
	const auto seconds = milliseconds / 1000.f;
	std::cout
		<< "Array 2D:\n\t "
		<< milliseconds
		<< " ms\n\t"
		<< steps / seconds
		<< " steps per second\n";
}


int main()
{
	constexpr auto rows = 1920;
	constexpr auto columns = 1080;

	std::unique_ptr<ILife<rows, columns>> array2d = std::make_unique<LifeArray2D<rows, columns>>();
	std::unique_ptr<ILife<rows, columns>> array = std::make_unique<LifeArray<rows, columns>>();
	std::unique_ptr<ILife<rows, columns>> bitset = std::make_unique<LifeBitset<rows, columns>>();

	RUN_LIFE(array2d);
	RUN_LIFE(array);
	RUN_LIFE(bitset);

	return 0;
}
